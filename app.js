const express = require('express');
const path = require('path');
const fs = require('fs');
const morgan = require('morgan');

const app = express();

const DIR_WITH_FILES = './files';

app.use(express.json()); // for parsing application/json
app.use(express.urlencoded({ extended: true })); // for parsing application/x-www-form-urlencoded

app.use(morgan('tiny'));

app.post('/api/files', createFile);
app.get('/api/files', getFiles);
app.get('/api/files/:filename', getFile);
app.delete('/api/files/:filename', deleteFile);
app.put('/api/files/:filename', modifyFile);

app.use(function(err, req, res, next) {
    if (err.status === 400) {
        res.json({
            message: 'Client error'
        });
    } else {
        console.error(err);
        res.status(500).json({
            message: 'Server error'
        });
    }
});

app.listen(8080);

function createFile (req, res) {
    const {filename, content, password} = req.body;
    (async () => {
        try {
            if (!fs.existsSync(DIR_WITH_FILES)) {
                fs.mkdirSync(DIR_WITH_FILES);
            }
            
            if (!filename) {
                throw "Please specify 'filename' parameter";
            }
            if (!content) {
                throw "Please specify 'content' parameter";
            }
            isFilenameValid(filename);

            const filePath = path.resolve(DIR_WITH_FILES, filename);
            isFileAlreadyExists(filePath);
            
            await fs.promises.writeFile(filePath, content, 'utf8');
            if (password) {
                setPassword(filename, password);
            }
            res.status(200).json({
                message: 'File created successfully'
            });
        } catch (err) {
            res.status(400).json({
                message: err
            });
        }
    })();
}

function getFiles (req, res) {
    (async () => {
        try {
            const files = await fs.promises.readdir(path.resolve(DIR_WITH_FILES));
            res.status(200).json({
                message: 'Success',
                files
            });
        } catch (err) {
            const message = err.code === 'ENOENT' ?
                            `Folder wasn't created. Please, create a file` : 
                            err.code;
            res.status(400).json({
                message
            })
        }
    })();
}

function getFile (req, res) {
    console.log(req.headers);
    const password = req.headers.authorization ? 
                    req.headers.authorization.match(/:(.+$)/)[1] :
                    null;
    console.log('password', password);

    const filePath = path.resolve(DIR_WITH_FILES, req.params.filename);
    (async () => {
        try {
            const content = await fs.promises.readFile(filePath, 'utf8');
            const extension = path.extname(filePath).replace(/^\./, '');
            const uploadedDate = fs.statSync(filePath).birthtime;
            res.status(200).json({
                message: 'Success',
                filename: req.params.filename,
                content,
                extension,
                uploadedDate
            })
        } catch (err) {
            const message = err.code === 'ENOENT' ?
                            `No file with '${req.params.filename}' filename found` : 
                            err.code;
            res.status(400).json({
                message
            })
        }
    })();
}

function deleteFile (req, res) {
    const filePath = path.resolve(DIR_WITH_FILES, req.params.filename);
    try {
        fs.unlinkSync(filePath);
        res.status(200).json({
            message: 'File deleted successfully'
        });
    } catch (err) {
        const message = err.code === 'ENOENT' ?
                            `No file with '${req.params.filename}' filename found` : 
                            err.code;
        res.status(400).json({
            message
        });
    }
}

function modifyFile (req, res) {
    const {filename, content} = req.body;
    const filePath = path.resolve(DIR_WITH_FILES, req.params.filename);
    try {
        if (!fs.existsSync(filePath)) {
            throw `File '${req.params.filename}' is absent`;
        }

        if (content) {
            fs.writeFileSync(filePath, content, 'utf8');  
        }

        if (filename) {
            const filePathNew = path.resolve(DIR_WITH_FILES, filename);
            if (filePathNew !== filePath) {
                isFilenameValid(filename);
                isFileAlreadyExists(filePathNew);
    
                fs.renameSync(filePath, filePathNew);
            }
        }

        res.status(200).json({
            message: 'File modified successfully'
        });
    } catch (err) {
        res.status(400).json({
            message: err
        });
    }
}

function isFileAlreadyExists (filename) {
    if (fs.existsSync(filename)) {
        throw 'File is already exists';
    }
    console.log('not Exists');
}

function isFilenameValid (filename) {
    if (!/^.+\.(log|txt|yaml|xml|js(on)?)$/i.test(filename)) {
        throw "Please change the extention in 'filename' parameter";
    }
    console.log('valid');
}

function isPasswordValid (password) {
    return (/.{4,}/).test(password + '');
}

function setPassword (filename, password) {
    console.log('setPassword', filename, password);
    const pathToStorage = path.resolve(DIR_WITH_FILES, '_storage');
    let storage = fs.existsSync(pathToStorage) ? fs.readFileSync(pathToStorage, 'utf8') : {};
    storage = JSON.parse(storage);
    storage[filename] = password;
    fs.writeFileSync(pathToStorage, JSON.stringify(storage), 'utf8');
}

function getPassword (filename) {
    if (!fs.existsSync(pathToStorage)) return null;
    const storage = fs.readFileSync(pathToStorage, 'utf8');
    return storage[filename] || null;
}